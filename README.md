# BevLab [![build](https://travis-ci.org/mattmatters/bevCollab.svg?branch=master)](https://travis-ci.org/mattmatters/bevCollab) [![maintain](https://api.codeclimate.com/v1/badges/dbb6f5f105b1b447ae1b/maintainability)](https://codeclimate.com/github/mattmatters/bevCollab/maintainability) [![coverage](https://api.codeclimate.com/v1/badges/dbb6f5f105b1b447ae1b/test_coverage)](https://codeclimate.com/github/mattmatters/bevCollab/test_coverage)

This is a work in progress web application that supports making, uploading, and sharing beer recipes.

## Running

```sh
npm install
npm start
```
